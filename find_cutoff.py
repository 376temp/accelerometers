from scipy import signal
from scipy.signal import resample

def find_cutoff_time(df):
    #find middle index
    idx_m = (df.index.values[-1] - df.index.values[0])/2
    
    #smooth acceleration
    b, a = signal.butter(3, 0.001, btype='lowpass', analog=False)
    low_pass_acc = signal.filtfilt(b,a,df['total'])
    
    #add to dataframe
    df['filtered'] = low_pass_acc
    
    #find  min/max indices
    min_ind = low_pass_acc[df['filtered'].values.argmin()]
    max_ind = low_pass_acc[df['filtered'].values.argmax()]

    #calc middle
    middle = (max_ind+min_ind)/2
    
    #cut in half
    dfl = df[df.index < idx_m]
    dfr = df[df.index > idx_m]
    
    #find absolute diff between middle and filtered data
    dfl['dif'] = (dfl['filtered'] - middle).abs()
    dfr['dif'] = (dfr['filtered'] - middle).abs()
    
    idxminl = dfl['dif'].argmin()
    idxminr = dfr['dif'].argmin()

    df = df[(df.index > idxminl) & (df.index < idxminr)]

    return df
