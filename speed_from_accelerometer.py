#!/usr/bin/env python

import pandas as pd
import numpy as np
from scipy import signal
from scipy.integrate import simps
from scipy.signal import resample
from statsmodels.nonparametric.smoothers_lowess import lowess
from find_cutoff import *

def setup_data(filename):
    """
    Sets up a dataframe with the data in the csv file called filename
    We make the following assumptions about the structure of the data:
    - There will be columns containing 'x', 'y', 'z', and 'time', for what you'd expect
    - The 'time' column is a time delta, i.e. some offset from 0
    """
    data = pd.read_csv(filename)

    # get data to look how everything else expects it
    xcol = data.filter(regex=r'(?i)x').columns[0]
    ycol = data.filter(regex=r'(?i)y').columns[0]
    zcol = data.filter(regex=r'(?i)z').columns[0]
    timecol = data.filter(regex=r'(?i)time').columns[0]
    data = data.rename(columns={xcol: 'x', ycol: 'y', zcol:'z', timecol: 'time'})

    # expect time to be in time delta format
    data['time'] = pd.to_timedelta(data['time'], unit='s')

    #data.set_index('time')
    data.index = data['time']

    # calculate the pre-resampling magnitude
    data['total'] = np.sqrt(data['x']**2 + data['y']**2 + data['z']**2)

    data = find_cutoff_time(data)

    return data

android_datasets = [
    'androidDataSets/100walkpocketeast-lac.csv',
    'androidDataSets/100pocket-lac1.csv', 
    'androidDataSets/100pocket-lac2.csv', 
    'androidDataSets/100pocket-lac3.csv',
    'androidDataSets/100pocket-lac4.csv'
]

iphone_datasets = [
    'iphoneDataSets/100walkpocketeastlac-lac.csv',
    'iphoneDataSets/100walkpocketwestlac-lac.csv' 
]

def datasets(directory=None, setup=setup_data):
    """
    Generates dataframes from the data we recorded.
    Optionally, can take data from directory and generate dataframes using a setup function
    If directory is provided, you must also provide setup. I don't know how your data looks.

    @param directory: str path to data directory
    @param setup: function accepting a filename and returning a dataframe with columns x, y, and z and a time-like index

    @yield result of setup and data source
    """
    if not directory:
        for filename in android_datasets:
            yield (setup(filename), 'android')

        for filename in iphone_datasets: 
            yield (setup(filename), 'iphone')

        raise StopIteration()
    else:
        for subdir, dirs, files in os.walk(rootdir):
            for file in files:
                yield (setup(f), directory)


def gather_approximations(data):
    resampled = data.resample("1S").mean()
    resampled['magnitude'] = np.sqrt(resampled['x']**2 + resampled['y']**2 + resampled['z']**2)

    # our data was collected in a controlled manner: we know we covered 100 meters
    ground_truth = 100 / resampled.shape[0]

    approximations = approximate_speed(resampled)
    return {'truth': ground_truth, 'approximations': approximations}

def approximate_speed(data):
    dt = data.shape[0]
    magnitude = data['magnitude'].dropna()

    baseline_approx = simps(magnitude)

    smoothed = lowess(magnitude.values, magnitude.index)
    loess_approx = simps(smoothed[:,1])

    b, a = signal.butter(5,.4, btype='low', analog=False)
    butter = signal.filtfilt(b, a, magnitude)
    butterworth_approx = simps(butter)

    return {
        'unfiltered': (baseline_approx, baseline_approx/dt),
        'loess': (loess_approx, loess_approx/dt),
        'butterworth': (butterworth_approx, butterworth_approx/dt)
    }

def main(filename, method='loess'):
    """
    Loads data in filename, computes distance covered and speed according to method,
    outputs results to stdout
    Possible methods: 'loess', 'unfiltered', 'butterworth'
    """
    data = setup_data(filename)
    approx = gather_approximations(data)['approximations']
    print("\nDistance: %f m\nSpeed: %f m/s" % approx[method])

if __name__ == '__main__':
    import sys
    argc = len(sys.argv)
    if argc >= 3:
        main(sys.argv[1], sys.argv[2])
    elif argc == 2:
        main(sys.argv[1])
    else:
        print("""Usage:
    data file: name of csv file containing your walking data. Must have time, x, y, z columns
    approx. method: 'loess' (default), 'butterworth', 'unfiltered'
    
    You can also play around in a Jupyter notebook:
    [1] for df in datasets():
            gather_approximations(df)""")

